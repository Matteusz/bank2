import java.util.Scanner;
class Szczegoly {

    private String acc_no;

    private String name;

    private String nazwisko;

    private String acc_type;

    private long balance;

    Scanner sc = new Scanner(System.in);

    public void openaccount() {
        System.out.print("Podaj numer konta: ");
        acc_no = sc.next();
        System.out.print("Podaj swoje imie: ");
        name = sc.next();
        System.out.print("Podaj swoje nazwisko: ");
        nazwisko = sc.next();
        System.out.print("Twój typ konta: ");
        acc_type = sc.next();
        System.out.print("Ilość pieniędzy na koncie: ");
        balance = sc.nextLong();
    }

    public void showaccount() {
        System.out.println("Imię i nazwisko właściciela: " + name + nazwisko);
        System.out.println("Numer konta: " + acc_no);
        System.out.println("Typ konta: " + acc_type);
        System.out.println("Stan konta: " + balance);
    }

    public void deposit() {
        long amt;
        System.out.println("Podaj jaką ilość chcesz wpłacić do banku: ");
        amt = sc.nextLong();
        balance = balance + amt;
    }

    public void withdraw() {
        long amt;
        System.out.print("Ile pieniędzy chcesz wypłacić?: ");
        amt = sc.nextLong();
        if (balance >= amt) {
            balance = balance - amt;
            System.out.print("Wypłacono pieniądze, stan konta po wypłaceniu: " + balance);
        } else {
            System.out.println("Brak wystarczających środków na koncie: " + amt);

        }
    }


    public boolean search(String ac_no) {
        if (acc_no.equals(ac_no)) {
            showaccount();
            return true;
        } else {
            return false;
        }


    }
}
